# THG-Scripts

Measurement scripts for the THG project.

Make sure to have the latest "starter.py", "starter_com.py" and example "tasks/task.py" files.

# Contents

- configuration: Different configurations for the dataloggers
- MeasurementScripts: Scripts for measurement
- "starters" contains files starting tasks for ease of use (double click)
- "tasks" contains tasks for the starter. See starter
