from devices.directors import StarterDirector

with StarterDirector(actor="starter") as c:
    c.start_tasks(("opoControl", "humidityReader", "wavemeterReader"))
