from devices.directors import StarterDirector

with StarterDirector(actor="starter") as c:
    c.start_tasks(("wavemeterReader", "polarizer_motors", "matching_motors"))
    # ?: "OPA1oven", "opoControl", "humidityReader", "republisher"
    # später: "fiberAmp", "koherasBasik"
