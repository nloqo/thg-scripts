"""
Start the necessary tasks for a spectroscopy measurement
"""


from pyleco.directors.starter_director import StarterDirector

# Parameters
tasks: list[str] = [
    "aculight",
    # "co2sensor",
    "fiberAmp",
    "koherasBasik",
    "matchingMotors",
    "MIRListener",
    "nicard",
    "OPA1oven",
    "opoControl",
    # "OPOoven",
    "polarizer",
    "republisher",
    # "siemens",
    "thyracont",
]


def main():
    with StarterDirector(actor="MYRES.starter") as d:
        d.start_tasks(tasks)


if __name__ == "__main__":
    main()
