import time
from PyTrinamic.connections.ConnectionManager import ConnectionManager
from PyTrinamic.modules.TMCM3110.TMCM_3110 import TMCM_3110  # works for TMCM6110 as well


from devices import NKTP

# bei 1064.00 maximum anfahren, und position festsetzen. dort specrometer scan für wellenlänge, anschließend seed thermisch zu minimum fahren und das hier ausführen

# Start conditions
idlerWavenumber = 2829.373
stepSize = 4  # number of half periods


class ThermalScan:
    def __init__(self, idlerWavenumber, *args, **kwargs):

        self.seed = NKTP.BASIK(COM=19, wlRange=[-380, 370])  # TODO

        self.idlerWavenumber = idlerWavenumber
        self.seedWavelength = self.seed.getWavelength()
        self.idlerWavelength = self.number2length(self.idlerWavenumber)
        self.signalWavelength = self.Idler2Signal(self.seedWavelength, self.idlerWavelength)

        self.config1 = {'motorNumber': 0,
                        'maxCurrent': 14,
                        'standbyCurrent': 2,
                        'positioningSpeed': 150,
                        'acceleration': 150,
                        'stepResolution': 7,
                        'stepCount': 24,
                        'unitSize': -0.024470478,
                        'unitOffset': 0,
                        'stallguardThreshold': 5}

        self.config2 = {'motorNumber': 1,
                        'maxCurrent': 14,
                        'standbyCurrent': 2,
                        'positioningSpeed': 150,
                        'acceleration': 150,
                        'stepResolution': 7,
                        'stepCount': 24,
                        'unitSize': -0.024470478,
                        'unitOffset': 0,
                        'stallguardThreshold': 5}

        self.theoreticalValue1 = self.polynomial1(self.idlerWavelength)
        self.theoreticalValue2 = self.polynomial2(self.idlerWavelength)

        self.c1 = -1.8626549723945685  # 16.3620803
        self.c2 = 1.7462227949341393  # 13.3644117 #

        self.motor1 = self.config1['motorNumber']
        self.motor2 = self.config2['motorNumber']
        self.connectMotors(True)

        self.velocity1 = self.config1["positioningSpeed"]
        self.velocity2 = self.config2["positioningSpeed"]

        self.configureMotor("motor1")
        self.configureMotor("motor2")
        self.setCurrentPosition()
        self.difference1 = self.theoreticalValue1 - self.start1
        self.difference2 = self.theoreticalValue2 - self.start2

        self.c0_1 = self.c1 - self.difference1
        self.c0_2 = self.c2 - self.difference2

    def configureMotor(self, motorName):
        """Configure the motor `motorname`."""
        print("Motor")
        connection = self.motors
        if motorName == "motor1":
            config = self.config1
        elif motorName == "motor2":
            config = self.config2
        try:
            motorNumber = config['motorNumber']
        except KeyError:
            return  # no value
        self.configureMotorTask(connection, motorNumber, config)

    def configureMotorTask(self, connection, motorNumber, config,
                           ):
        print("HellO", config['maxCurrent'])
        """Task for configuring a motor."""
        try:
            connection.setMaxCurrent(motorNumber, round(2.55 * config['maxCurrent']))
            connection.setMotorStandbyCurrent(motorNumber, round(2.55 * config['standbyCurrent']))
            connection.setMaxVelocity(motorNumber, config['positioningSpeed'])
            connection.setMaxAcceleration(motorNumber, config['acceleration'])
            connection.setAxisParameter(connection.APs.MicrostepResolution,
                                        motorNumber, config['stepResolution'])
            connection.setAxisParameter(connection.APs.SG2Threshold, motorNumber,
                                        config['stallguardThreshold'])
        except KeyError:
            pass  # no value
        except Exception as exc:
            print(exc)

    def connectMotors(self, checked):
        '''Connect the stepmotor card.'''
        if checked:
            COM = 8
            self.motorsConnectionManager = ConnectionManager(f"--port COM{COM}")
            self.motors = TMCM_3110(self.motorsConnectionManager.connect())
            self.switchSets = (
                (self.motors.APs.RightLimitSwitchDisable, 'RightLimitSwitchDisable', 0),
                (self.motors.APs.LeftLimitSwitchDisable, 'LeftLimitSwitchDisable', 0),
                (self.motors.APs.SoftStopFlag, 'SoftStopFlag', 0),
                (self.motors.APs.EndSwitchPowerDown, 'EndSwitchPowerDown', 0),
                (self.motors.APs.ReferenceSearchSpeed, 'ReferenceSearchSpeed', 20))

            for setting in self.switchSets:
                apType, name, value = setting
                self.motors.setAxisParameter(apType, self.motor1, value)
                self.motors.setAxisParameter(apType, self.motor2, value)

        else:
            try:
                self.motorsConnectionManager.disconnect()
            except AttributeError:
                pass

    def setCurrentPosition(self):
        '''Set the horizontal and vertical center to the actual position of the motors (-->Target start position = actual position)'''
        print(self.motors.getActualPosition(self.motor1), self.config1)
        self.start1 = (self.stepsToUnits(self.motors.getActualPosition(self.motor1), self.config1))
        self.start2 = (self.stepsToUnits(self.motors.getActualPosition(self.motor2), self.config2))

    def stepsToUnits(self, microsteps, config):
        """Convert `microsteps` to a unit according to the motor `config`."""
        stepResolution = 2 ** config['stepResolution']  # microsteps per fullstep
        fStepsPerRev = config['stepCount']  # fullstep per revolution
        unitSize = config['unitSize']  # unit per revolution
        offset = config['unitOffset']  # offset in units
        return (microsteps / stepResolution / fStepsPerRev * unitSize) + offset

    def unitsToSteps(self, units, config):
        """Convert `units` to microsteps according to motor `config`."""
        stepResolution = 2 ** config['stepResolution']  # microsteps per fullstep
        fStepsPerRev = config['stepCount']  # fullstep per revolution
        unitSize = config['unitSize']  # unit per revolution
        offset = config['unitOffset']  # offset in units
        return round((units - offset) / unitSize * fStepsPerRev * stepResolution)

    def setWavelength(self):
        """Set the seed wavelength."""
        try:
            assert (self.seed.wlRange[0] / 1000) <= self.setpoint - 1064 and self.setpoint - 1064 <= (self.seed.wlRange[1] / 1000), "Invalid range for seed wavelength."
        except AssertionError:
            if self.setpoint < 1064:
                self.setpoint = 1064 + (self.seed.wlRange[0] / 1000)
            else:
                self.setpoint = 1064 + (self.seed.wlRange[1] / 1000)
        self.seed.setWavelengthSetpoint(self.setpoint)

    def setModulationPressed(self):
        """Set the modulation config via Communicator."""
        data = {}
        newFrequency = 0.01
        data['frequency'] = newFrequency
        self.wavelengthModulationFrequency = newFrequency
        data['level'] = 1000
        data['offset'] = 0
        self.setModulation(data=data, seed=self.seed)

    def setModulation(self, seed, data):
        """Set the modulation config."""
        try:
            if 'frequency' in data.keys():
                seed.setWavelengthModulationFrequency(data['frequency'], 0)
                seed.setModulationSetup({'wavelengthFrequency': 0})
            seed.setWavelengthModulationLevel(data['level'])
            seed.setWavelengthModulationOffset(data['offset'])
        except ConnectionError as exc:
            print(('seed', "setModulation", exc))

    def setWavelengthStep(self):
        """Do a single wavelength step."""
        # 10 GHz corresponds to 5 pm
        self.setpoint -= 0.03
        self.setWavelength()

    def setWavelengthMin(self):
        """Set the value to the minimum."""
        self.setpoint = (self.seed.wlRange[0] / 1000) + 1064
        self.setWavelength()

    def setWavelengthMax(self):
        """Set the value to the minimum."""
        self.setpoint = (self.seed.wlRange[1] / 1000) + 1064
        self.setWavelength()

    def polynomial1(self, wavelength, c0_1=-1.8626549723945685):  # 16.3620803):
        c1 = 0.006844062914328953
        # c1 = -0.00351682
        c2 = -1.4739192892921605e-06
        return c0_1 + c1 * wavelength + c2 * wavelength**2

    def polynomial2(self, wavelength, c0_2=1.7462227949341393):  # :13.3644117
        c1 = 0.0030972435728289544
        # c1 = -0.00353415
        c2 = -9.397764478275087e-07
        return c0_2 + c1 * wavelength + c2 * wavelength**2

    def Idler2Signal(self, lambdaPump, lambdaIdler):
        lambdaSignal = 1 / (1 / lambdaPump - 1 / lambdaIdler)
        return lambdaSignal

    def Signal2Idler(self, lambdaPump, lambdaSignal):
        lambdaIdler = 1 / (1 / lambdaPump - 1 / lambdaSignal)
        return lambdaIdler

    def number2length(self, wavenumber):
        return 1 / ((10 ** (-9) * wavenumber) * 100)

    def length2number(self, wavelength):
        return 1 / ((10 ** (-9) * wavelength) * 100)


Scan = ThermalScan(idlerWavenumber)  # 1064 :2821.726  , 1064.3: 2818.801, 1063.7: 2823.754

# dauert bei 10 sek messzeit, 0.005 SChritten circa 28 min, 40 Minuten bei 15 sek Messzeit
# Scan.setWavelengthMin()#rausnehmen, wenn mit beamprofiler

Scan.setModulationPressed()
Scan.setpoint = Scan.seed.getWavelength()
start = Scan.seed.getWavelength()


try:
    while Scan.setpoint > 1064 - (370 / 1000):
        print("Wellenlänge", "Ziel", Scan.setpoint, "Aktuell", Scan.seed.getWavelength())
        if True:  # Scan.seed.getWavelength()>start:
            print("True")
            currentWavelength1 = (Scan.Signal2Idler(Scan.setpoint, Scan.signalWavelength))

            currentWavelength2 = Scan.Signal2Idler(start, Scan.signalWavelength) + 1.25 * (Scan.Signal2Idler(Scan.setpoint, Scan.signalWavelength) - Scan.Signal2Idler(start, Scan.signalWavelength))
        target1 = Scan.unitsToSteps(Scan.polynomial1(currentWavelength1, Scan.c0_1), Scan.config1)
        target2 = Scan.unitsToSteps(Scan.polynomial2(currentWavelength2, Scan.c0_2), Scan.config2)
        print("Target im Zentrum", Scan.unitsToSteps(Scan.polynomial1(Scan.Signal2Idler(1064, Scan.signalWavelength), Scan.c0_1), Scan.config1))
        print("Target im Zentrum", Scan.unitsToSteps(Scan.polynomial2(Scan.Signal2Idler(1064, Scan.signalWavelength), Scan.c0_2), Scan.config2))

        Scan.motors.moveTo(Scan.motor1, target1, velocity=Scan.velocity1)
        Scan.motors.moveTo(Scan.motor2, target2, velocity=Scan.velocity2)
        while not (Scan.motors.positionReached(Scan.motor1) and Scan.motors.positionReached(Scan.motor2)):
            pass
        print("Motor1", "Target", target1, "Ist", Scan.motors.getActualPosition(Scan.motor1))
        print("Motor2", "Target", target2, "Ist", Scan.motors.getActualPosition(Scan.motor2))

        Scan.setWavelengthStep()
        time.sleep(50 * stepSize)

finally:
    Scan.connectMotors(False)
