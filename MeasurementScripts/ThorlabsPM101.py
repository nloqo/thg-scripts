# -*- coding: utf-8 -*-
"""
Created on Tue Aug  9 11:38:53 2022

@author: THG-User
"""

from time import sleep

from pymeasure.instruments.thorlabs import ThorlabsPM100USB
from pyleco.utils.data_publisher import DataPublisher

portKeller = "USB::0x1313::0x8078::P0006836"  # Keller
portFP = "USB::0x1313::0x8078::P0028035"  # FP


interval = 50  # ms
port = portFP

if __name__ == "__main__":
    publisher = DataPublisher(full_name="MYRES.PM100D")
    pm = ThorlabsPM100USB(port)
    print("connected")
    try:
        while True:
            publisher.send_data({'power': pm.power})
            sleep(interval / 1000)
    except KeyboardInterrupt:
        pass
