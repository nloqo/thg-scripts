"""
Do some scans
"""

# %% Imports

from time import sleep

import numpy as np
from pyleco.utils.communicator import Communicator
from pyleco.directors.transparent_director import TransparentDirector
from pyleco.directors.data_logger_director import DataLoggerDirector, TriggerTypes, ValuingModes
from pyleco.utils.publisher import Publisher


# %% General setup

saved_files: list[str] = []


# %% Directors

communicator = Communicator(name="ScanDirector")

dlDirector = DataLoggerDirector(actor="DataLoggerScan", communicator=communicator)
tDirector = TransparentDirector(communicator=communicator)

publisher = Publisher()


# %% Scan definition

def scan(scan_parameter_name: str,
         steps,
         setup_method,
         do_step,
         add_subscriptions: list[str],
         cleanup_method,
         interval: float = 0,
         valuing_mode=ValuingModes.LAST,
         ) -> str:
    subscriptions = ["time", scan_parameter_name] + add_subscriptions
    # Setup
    setup_method()
    dlDirector.start_collecting(trigger_type=TriggerTypes.VARIABLE,
                                trigger_value=scan_parameter_name,
                                subscriptions=subscriptions, valuing_mode=valuing_mode)

    for step in steps:
        do_step(step)
        sleep(interval)
        publisher({scan_parameter_name: step})

    name = dlDirector.saveData()
    saved_files.append(name)
    print(f"File saved as {name}")
    cleanup()
    return name


# %% Different scans

# %%% Etalon scan

# General parameters
scan_parameter_name: str = "etalon"
subscriptions: list[str] = [
    "signal_wl",
    "signal_P",
]
steps = np.arange(-2.65, -2.75, -.002)


def setup():
    tDirector.actor = "nicard"


def do_step(step):
    tDirector.etalon = step


def cleanup():
    pass


scan(scan_parameter_name="etalon", steps=steps, setup_method=setup, do_step=do_step,
     cleanup_method=cleanup, valuing_mode=ValuingModes.AVERAGE, add_subscriptions=subscriptions,
     interval=1)


# %%% OPO crystal position scan

scan(scan_parameter_name="xyz",
     add_subscriptions=subscriptions)



# %%%

name = dlDirector.saveData()
