# -*- coding: utf-8 -*-
"""
Listening for MIR Lasersystem measurements. 'MIRListener'
"""

import logging
from time import perf_counter
from typing import Any, Optional, Union

from zmq.sugar import Context

from pyleco.utils.extended_message_handler import ExtendedMessageHandler


log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())


# Parameters
interval = 0.05  # Readout interval in s
difference = 3  # s
variables = {
    # name: limit
    "signal_P": 0.3,  # µW
    "idler2_E": 1e-6,  # J
}


class ListeningHandler(ExtendedMessageHandler):
    values: dict[str, tuple[float, float]]

    def __init__(
        self,
        name: str,
        variables: dict[str, float],
        context: Optional[Context] = None,
        host: str = "localhost",
        data_host: Optional[str] = None,
        **kwargs,
    ) -> None:
        super().__init__(name, context, host, data_host, **kwargs)
        now = perf_counter()
        self.variables = variables
        self.values = {var: (now, float("nan")) for var in variables}
        self.subscribe(variables.keys())

    def register_rpc_methods(self) -> None:
        super().register_rpc_methods()
        self.register_rpc_method(self.get_parameters)

    def handle_subscription_data(self, data: dict[str, Any]) -> None:
        now = perf_counter()
        for key, value in data.items():
            if key in self.variables:
                self.values[key] = (now, float(value))

    def get_parameters(self, parameters: Union[list[str], tuple[str, ...]]) -> dict[str, Any]:
        """Get device properties from the list `properties`."""
        data = {}
        now = perf_counter()
        for key in parameters:
            try:
                time, value = self.values[key]
                limit = self.variables[key]
            except KeyError:
                continue
            if now < time + difference:
                data[key] = value > limit
            else:
                data[key] = None
        return data


def task(stop_event) -> None:
    """The task which is run by the starter."""
    with ListeningHandler("MIRListener", variables=variables) as listener:
        listener.listen(stop_event=stop_event)


if __name__ == "__main__":
    """Run the task if the module is executed."""
    log.addHandler(logging.StreamHandler())
    log.setLevel(logging.INFO)

    class Signal:
        def is_set(self):
            return False

    try:
        task(Signal())
    except KeyboardInterrupt:
        pass
