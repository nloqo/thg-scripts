# -*- coding: utf-8 -*-
"""
Motor controller for phase matching motors. 'matchingMotors'
"""


import logging

from pyleco_extras.actors.tmc_motor_actor import TMCMotorActor

log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())


# Parameters
configs = [
    {'motorNumber': 0,  # Default
     'maxCurrent': 50,
     'standbyCurrent': 5,
     'positioningSpeed': 1024,
     'acceleration': 512,
     'stepResolution': 3,
     'stepCount': 200,
     'unitSize': 360,
     'unitSymbol': "°",
     'unitOffset': 0,
     'stallguardThreshold': 0,
     },
    {'motorNumber': 2,
     'maxCurrent': 50,
     'standbyCurrent': 5,
     'positioningSpeed': 512,
     'acceleration': 512,
     'stepResolution': 3,
     'stepCount': 200,
     'unitSize': 0.25,
     'unitSymbol': "mm",
     'unitOffset': 0,
     'stallguardThreshold': 0,
     },
    {'motorNumber': 3,
     'maxCurrent': 50,
     'standbyCurrent': 5,
     'positioningSpeed': 512,
     'acceleration': 512,
     'stepResolution': 3,
     'stepCount': 200,
     'unitSize': 0.25,
     'unitSymbol': "mm",
     'unitOffset': 0,
     'stallguardThreshold': 0,
     },
    # {'motorNumber': 3,  # OPO
    #  'maxCurrent': 48,
    #  'standbyCurrent': 5,
    #  'positioningSpeed': 50,
    #  'acceleration': 512,
    #  'stepResolution': 3,
    #  'stepCount': 200,
    #  'unitSize': 0.25,
    #  'unitSymbol': "mm",
    #  'unitOffset': 0,
    #  'stallguardThreshold': 0,
    #  },
    {'motorNumber': 4,  # OPA2a
     'maxCurrent': 20,
     'standbyCurrent': 2,
     'positioningSpeed': 50,
     'acceleration': 35,
     'stepResolution': 7,
     'stepCount': 24,
     'unitSize': 729 / 29791,
     'unitSymbol': "mm",
     'unitOffset': 0,
     'stallguardThreshold': 0,
     },
    {'motorNumber': 5,  # OPA2b
     'maxCurrent': 20,
     'standbyCurrent': 2,
     'positioningSpeed': 50,
     'acceleration': 35,
     'stepResolution': 7,
     'stepCount': 24,
     'unitSize': 729 / 29791,
     'unitSymbol': "mm",
     'unitOffset': 0,
     'stallguardThreshold': 0,
     },
]
# Dictionary with names for the motors
motorDict = {
    'OPO': 3,
    'OPA2a': 4,
    'OPA2b': 5,
}
# Name to communicate with
name = "matchingMotors"
# Name of the card or COM port
card = "MatchingMotors"


def task(stop_event):
    """The task which is run by the starter."""
    # Initialize
    try:
        controller = TMCMotorActor(name, card, motorDict, log=log)
    except Exception as exc:
        log.exception(f"Creation of {name} at card {card} failed.", exc_info=exc)
        return
    for config in configs:
        controller.configure_motor(config)

    # Continuous loop
    controller.listen(stop_event)

    # Finish
    controller.disconnect()
    controller.close()


if __name__ == "__main__":
    # Run the task if executed.
    class Signal:
        def is_set(self):
            return False
    try:
        task(Signal())
    except KeyboardInterrupt:
        pass
