"""
Start the Lasersystem at a programmed time. "autostarter"
"""

from datetime import datetime
import logging
from time import sleep
from typing import Optional

from qtpy import QtCore
from zmq import Poller
from zmq.sugar import Socket

from pyleco.directors.transparent_director import TransparentDirector
from pyleco.directors.starter_director import StarterDirector
from pyleco.utils.message_handler import MessageHandler

# Parameters
start_timestamp: Optional[str] = None  # "2023-11-23T07:30"  # has to be in the future

# Actor names
starter = "MYRES.starter"
seed = "MYRES.koherasBasik"
amplifier = "MYRES.fiberAmp"
OPO2Oven = "MYRES.OPOoven"
OPOBlock = "MYRES.opoControl"
OPA1Oven = "MYRES.OPA1oven"
polarizer = "MYRES.polarizer"
matchingMotors = "MYRES.matchingMotors"
nicard = "MYRES.nicard"
seedShutter = "pico3.seedShutter"
YAGseed = "pico3.innolas"
YAGshutter = "pico3.yagShutter"


log = logging.getLogger(__name__)


class TimedHandler(MessageHandler):
    start_time: datetime
    _power_setpoint: float

    def __init__(self, name: str, host: str = "localhost",
                 start_time: Optional[str | datetime] = None,
                 **kwargs):
        super().__init__(name, host, **kwargs)
        settings = self.get_settings_object()
        if start_time is None:
            start_time = settings.value("start_time", "2023-11-23T07:30")
        if isinstance(start_time, datetime):
            pass
        elif isinstance(start_time, str):
            start_time = datetime.fromisoformat(start_time)
        else:
            raise ValueError("start_time has to be a timestamp or None.")
        self.start_time = start_time
        self.started = datetime.now() > self.start_time
        self._stopping = False
        self._power_setpoint = settings.value("power_setpoint", 0, float)

    def register_rpc_methods(self) -> None:
        super().register_rpc_methods()
        self.register_rpc_method(self.is_started)
        self.register_rpc_method(self.set_start_date)
        self.register_rpc_method(self.get_start_date)
        self.register_rpc_method(self.set_start_power)
        self.register_rpc_method(self.get_start_power)
        self.register_rpc_method(self.start_laser)

    @staticmethod
    def get_settings_object():
        return QtCore.QSettings("NLOQO", "autostarter")

    def _listen_loop_element(self, poller: Poller, waiting_time: int | None) -> dict[Socket, int]:
        self.test_for_start_stop()
        return super()._listen_loop_element(poller, waiting_time)

    def test_for_start_stop(self) -> None:
        if self.started is False and datetime.now() > self.start_time:
            self.start_laser(self._power_setpoint)

    def start_laser(self, power_setpoint: Optional[float] = None) -> None:
        if power_setpoint is None:
            power_setpoint = self._power_setpoint
        log.info(f"Starting the laser system as scheduled with {power_setpoint} W.")
        with StarterDirector(actor=starter, communicator=self) as director:
            director.start_tasks([name.split(".")[-1] for name in (seed, amplifier)])
        sleep(0.5)
        with TransparentDirector(communicator=self) as director:
            director.actor = amplifier
            director.device.power_setpoint = power_setpoint  # type: ignore
        self.started = True

    def is_started(self) -> bool:
        return self.started

    def stop_laser(self) -> None:
        with TransparentDirector(communicator=self) as director:
            director.actor = amplifier
            director.device.power_setpoint = -2  # type: ignore

    def set_start_date(self, start_date: str) -> None:
        start_time = datetime.fromisoformat(start_date)
        if start_time < datetime.now():
            raise ValueError("`start_time` has to be in the future.")
        self.start_time = start_time
        self.get_settings_object().setValue("start_time", start_date)
        self.started = False

    def get_start_date(self) -> str:
        return self.start_time.isoformat()

    def set_start_power(self, power: float) -> None:
        self._power_setpoint = power
        self.get_settings_object().setValue("power_setpoint", power)

    def get_start_power(self) -> float:
        return self._power_setpoint


def task(stop_event):
    """The task which is run by the starter."""
    # Initialize
    handler = TimedHandler(name="autostarter", start_time=start_timestamp)

    # Continuous loop
    handler.listen(stop_event=stop_event)

    # Finish
