# -*- coding: utf-8 -*-
"""
Read the high_finesse Wavemeters. "wavemeterReader"
"""


import logging
import time

from devices.high_finesse import Wavemeter, wlmConst
from pyleco.actors.actor import Actor
from pyleco.utils.data_publisher import DataPublisher

log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())


# Parameters
devices = {3945: "signal", 598: "pump"}  # dictionary of the serial numbers and publishing names
default_name = "wavemeter"  # default name if serial number is unknown.
interval = 1  # s, interval between check for over/under-exposure or time in normal mode
auto_exposure = False  # adjust automatically the exposure time
switching_enabled: bool = False  # Switch between both wavelength regions.
alternative_name: str = "pumpSHG"  # for values below 1000 nm
alternative_interval: int = 30


class WavemeterActor(Actor[Wavemeter]):
    last_wl: dict[int, float] = {}

    def callback(self, version: int, mode: int, intVal: int, dblVal: float, Res1=None):
        """Callback method."""
        if mode in (wlmConst.cmiWavelength1,
                    wlmConst.cmiWavelength2,
                    wlmConst.cmiWavelength3,
                    wlmConst.cmiWavelength4,
                    wlmConst.cmiWavelength5,
                    wlmConst.cmiWavelength6,
                    ):
            self.show_wavelength(version, intVal, dblVal)
            # log.debug(("Wavelength received", version, mode, intVal, dblVal))
        elif mode in (wlmConst.cmiFrequency1,
                      wlmConst.cmiFrequency2,
                      ):
            self.show_frequency(version, intVal, dblVal)
            # log.debug(("Frequency received", version, mode, intVal, dblVal))
        elif mode == wlmConst.cmiPower:
            self.show_power(version, intVal, dblVal)
            # log.debug(("Power received", version, mode, intVal, dblVal))
        else:
            log.debug(("Data received", version, mode, intVal, dblVal))

    def show_wavelength(self, version: int, timestamp: int, value: float):
        """Show the received wavelength."""
        name = devices.get(version, default_name)
        value = value if value > 0 else float("nan")
        self.publisher.send_legacy({f"{name}_wl": value if value > 0 else float("nan")})
        self.last_wl[version] = value

    def show_frequency(self, version: int, timestamp: int, value: float):
        """Show the received frequency."""
        name = devices.get(version, default_name)
        self.publisher.send_legacy({f"{name}_f": value})

    def show_power(self, version: int, timestamp: int, value: float):
        """Show the received power."""
        name = devices.get(version, default_name)
        self.publisher.send_legacy({f"{name}_P": value})

    def read_publish(self, device: Wavemeter, publisher: DataPublisher) -> None:
        if auto_exposure:
            try:
                power = device.power
            except Exception as exc:
                log.exception("Reading power failed.", exc_info=exc)
            else:
                if power == float("inf") or power == float("nan"):
                    device.auto_exposure_enabled = True
                else:
                    device.auto_exposure_enabled = False
        if switching_enabled:
            pass  # TODO fix
            # if next_loop == interval:
            #     # switch to lower range
            #     device.range = 6
            #     devices[3945] = alternative_name
            #     next_loop = alternative_interval
            # else:
            #     # switch to upper range:
            #     device.range = 7
            #     devices[3945] = original_name
            #     next_loop = interval


def task(stop_event):
    """The task which is run by the starter."""
    # Initialize
    with WavemeterActor("wavemeterReader", Wavemeter, periodic_reading=interval) as a:
        a.connect()
        a.device.setup_callback(a.callback)
        log.info(f"Wavemeter reader started. {a.device.wlm_count} wavemeters found.")

        # Continuous loop
        a.listen()

        # Finish
        a.device.setup_callback()


if __name__ == "__main__":
    """Run the task if the module is executed."""
    if len(log.handlers) < 2:
        log.addHandler(logging.StreamHandler())
    log.setLevel(logging.INFO)

    class Signal:
        def is_set(self):
            return False

        def wait(self, interval):
            time.sleep(interval)
            return False

    try:
        task(Signal())
    except KeyboardInterrupt:
        pass
