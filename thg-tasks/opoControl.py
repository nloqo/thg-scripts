# -*- coding: utf-8 -*-
"""
Read the temperature of the OPO temperature controller arduino. "opoControl"

Created on Thu Dec 15 13:54:36 2022

@author: moneke
"""

import logging

from pyleco.actors.actor import Actor, DataPublisher
try:
    from pymeasure.instruments.resources import find_serial_port
except ImportError:
    from devices.findDevices import find_serial_port

from devices.arduino import ControllerArduino
from devices.tdk_ntc import ntc_sh


log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())


# Parameters
hw_info = 0x2341, 0x0043, "85235333035351904230"
names = ["OPOBlockStab", "OPOBlock", "OPOBoxOutside"]  # to publish under
interval = 0.2  # seconds
read_integral: bool = True
integral_name = "OPOBlockIntegral"
# Start values for integral and setpoint after reconnecting
integral_start_value: float | None = None  # start integral_value
setpoint: float | None = 389.5  # start setpoint in internal units (higher = colder)
# None uses value stored in EEPROM (currently: 385)
# Temperature in °C: internal value
# 35.45: 400
# 36.0: 394.5
# 36.45: 390
# 36.5: 389.5
# 36.961: 385


converter = ntc_sh.NTC_SH(((10, 19900), (30, 8057), (60, 2488)))
if read_integral:
    names = [integral_name] + names


class TemperatureControllerArduino(ControllerArduino):
    """Control the temperature"""

    def readout_temperatures(self):
        """Read the Arduino values and return temperatures in °C."""
        try:
            # Get relative volts.
            volts = self.data
        except Exception as exc:
            log.exception("Readout failed.", exc_info=exc)
            return []
        try:
            values = [converter.temperature(1e4 * v / (1 - v)) for v in volts[:7]]
        except ValueError as exc:
            log.exception("Value error on temperature calculation.", exc)
            return []
        # originally for humidity sensor
        # try:
        #     values.append(1064 * volts[7])
        # except IndexError:
        #     pass  # not enough data received
        return values

    @property
    def temperatures(self) -> list[float]:
        return self.readout_temperatures()


def read_publish(device: TemperatureControllerArduino, publisher: DataPublisher):
    if read_integral:
        values = [device.get_pid_values()[0]]
    else:
        values = []
    values.extend(device.temperatures)
    data = {}
    for i in range(min(len(names), len(values))):
        if names[i]:
            data[names[i]] = values[i]
    log.debug(data)
    try:
        publisher.send_legacy(data)
    except Exception as exc:
        log.exception("Publisher error.", exc_info=exc)


def task(stop_event):
    with Actor("opoControl", TemperatureControllerArduino, periodic_reading=interval) as actor:
        actor.read_publish = read_publish
        port = find_serial_port(*hw_info)
        actor.connect(adapter=port, visa_library="@py")
        if setpoint is not None:
            actor.device.setpoint = setpoint
        if integral_start_value is not None:
            actor.device.integral_value = integral_start_value
        # Loop
        actor.listen(stop_event=stop_event)
        # Finish (in exit)


if __name__ == "__main__":
    # Run the task if executed.
    class Signal:
        def is_set(self):
            return False
    try:
        task(Signal())
    except KeyboardInterrupt:
        pass
