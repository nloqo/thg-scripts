# -*- coding: utf-8 -*-
"""
Read the Thyracont pressure sensor
"""

import logging

import numpy as np
from pyleco.utils.data_publisher import DataPublisher
from pyleco.actors.actor import Actor

try:
    from pymeasure.instruments.resources import find_serial_port  # type: ignore
except ImportError:
    from devices.findDevices import find_serial_port
from pymeasure.instruments.thyracont.smartline_v2 import VSR


log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())


# Configuration
hw_info = 0x0403, 0x6001, "AB0P5EA1A"  # broken adapter: "AB0LHRGWA"
detailed = True  # show also the piezo and pirani pressure
HCl_calibration = True  # show HCl calibrated data
interval = 0.05  # s


def calculate_HCl_pressure(p: float) -> float:
    """Calculate the HCl pressure in mbar based on calibrated N2 pressure in mbar.

    Based on:
    - "Verfahren zur Kalibrierung des Thyracont Sensors auf HCl" of 2022-01-18 by Oskar Ernst
    - "Stand Detektions-System SFM/THG" of 2022-1-24 by Benedikt Burger
    """

    def exponentialPlus(x, a, b, c, d):
        return a * x**b + c + d * x

    # pressure limits
    limit1 = 16.04628
    limit2 = 1.2592278
    limit3 = 0.153085

    if limit1 <= p:
        c = p
    elif limit2 <= p:
        c = exponentialPlus(p, 7.70885685,  0.34144037, -3.83997801, 0)
    elif limit3 <= p:
        c = exponentialPlus(p, 1.40091029,  3.87190482, -0.03701845,  0.88537411)
    elif p < limit3:
        c = .65 * p
    elif np.isnan(p):
        c = np.nan
    else:
        c = np.nan
    return c


def readout(device: VSR, publisher: DataPublisher) -> None:
    """Do regular readout"""
    try:
        thyracont: float = device.pressure  # type: ignore
        if detailed:
            thyracontPiezo = device.piezo.pressure  # type: ignore
            thyracontPirani = device.pirani.pressure  # type: ignore
    except AttributeError:
        pass
    except ConnectionResetError as exc:
        log.debug(f"Thyracont timeout: {exc}")
    except (ConnectionError, AssertionError) as exc:
        log.warning(f"Get pressure error {type(exc).__name__}: '{exc}'")
    except Exception as exc:
        log.exception("Communication error.", exc_info=exc)
    else:
        data: dict[str, float] = {"precisionPressure": thyracont}
        if detailed:
            data["thyracontPiezo"] = thyracontPiezo
            data["thyracontPirani"] = thyracontPirani
        if HCl_calibration:
            data["HCl_pressure"] = calculate_HCl_pressure(thyracont)
        publisher.send_legacy(data)


def task(stop_event):
    """The task which is run by the starter."""
    # Initialize
    com = find_serial_port(*hw_info)
    with Actor(name="thyracont", device_class=VSR, periodic_reading=interval) as actor:
        actor.read_publish = readout
        actor.connect(com)
        actor.listen(stop_event=stop_event)
