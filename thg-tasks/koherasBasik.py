# -*- coding: utf-8 -*-
"""
Controlling the NKT BASIK seed laser. "koherasBasik"

This script cannot be restarted, unless the starter is restarted as well, as the opened ports are
not closed correctly.
"""


import logging

from devices.NKTP import BASIK

from pyleco.actors.actor import Actor, DataPublisher
try:
    from pymeasure.instruments.resources import find_serial_port
except ImportError:
    from devices.findDevices import find_serial_port


log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())


# Parameters
interval = 0.04  # s regular readout
variable_name = 'seedWavelengthCalculated'


def readout(device: BASIK, publisher: DataPublisher):
    publisher.send_legacy({variable_name: device.wavelength})


def task(stop_event):
    """The task which is run by the starter."""
    # Initialize
    with Actor("koherasBasik", BASIK, periodic_reading=interval, log=log) as a:
        a.read_publish = readout
        try:
            a.connect(find_serial_port(4292, 60000, "2036G355").replace("ASRL", "COM"),
                      wlRange=[-380, 370])
        except Exception as exc:
            log.exception("Connection failed.", exc_info=exc)
            return
        try:
            a.device.setPorts()
        except ConnectionError as exc:
            log.exception("Set port failed with ConnectionError.", exc_info=exc)
        a.device.wavelength_modulation_frequency2 = 0

        # Loop
        try:
            a.listen(stop_event)
        finally:
            try:
                a.device.closePorts()
            except ConnectionError:
                log.exception("Closing ports failed.")

        # Close (in exit)


if __name__ == "__main__":
    # Run the task if executed.
    log.addHandler(logging.StreamHandler())
    log.setLevel(logging.INFO)

    class Signal:
        def is_set(self):
            return False
    try:
        task(Signal())
    except KeyboardInterrupt:
        pass
