"""
Control the NI card. 'nicard'
"""

import logging
from threading import Event
from typing import Any, Callable, Optional

import nidaqmx
import numpy as np
from pyleco.actors.actor import Actor, DataPublisher

log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())

# Parameters
interval = 0.05  # s
output_variable_name = "OPOEtalon"
averaging = 10


def property_creator(
    channel: int,
    averaging: int = averaging,
    calibration: Optional[Callable[[float], float]] = None,
    doc: str = "",
    fset: Optional[Callable[[Any, Any], None]] = None,
    **kwargs,
) -> property:
    """Create a read property."""

    def fget(self) -> float:
        with nidaqmx.Task() as niTask:
            niTask.ai_channels.add_ai_voltage_chan(f"Dev1/ai{channel}", **kwargs)
            raw: float = np.mean(niTask.read(number_of_samples_per_channel=averaging))  # type: ignore  # noqa
        return raw if calibration is None else calibration(raw)

    fget.__doc__ = doc
    return property(fget=fget, fset=fset)


# Calibration methods for input/output
def cell_pressure_calibration(voltage: float) -> float:
    return voltage * 124.91118096 - 246.40557883


def panel_pressure_calibration(voltage: float) -> float:
    return voltage * 199.85568833 - 396.06299701


def heinzinger_read_calibration(voltage: float) -> float:
    """Calculate the HV output in Volts depending on the measured signal in Volts."""
    return (voltage + 0.035127) / 0.0016706


def heinzinger_write_calibration(high_voltage: float) -> float:
    """Calculate the necessary control voltage in Volts to generate HV output in Volts."""
    return high_voltage * 0.0016706 - 0.035127


class NiCardInstrument:
    """Instrument compatible with pymeasure actor controlling a NI card."""

    def __init__(self, adapter=None, name="NI card", publisher: DataPublisher | None = None):
        self.name = name
        self._output = float("nan")
        self.publisher = publisher or DataPublisher(full_name=name)
        self.adapter = self.Adapter()

    class Adapter:
        """Contain all the connections, to close them."""
        def __init__(self):
            self.setup_siemens_pressure_sensors()

        def setup_siemens_pressure_sensors(self):
            self.siemens_task = niTask = nidaqmx.Task()
            niTask.ai_channels.add_ai_voltage_chan("Dev1/ai0", max_val=10)
            niTask.ai_channels.add_ai_voltage_chan("Dev1/ai1", max_val=10)

        def close(self):
            self.siemens_task.close()

    # Input
    cell_pressure = property_creator(
        channel=0,
        max_val=10,
        calibration=cell_pressure_calibration,
        doc="Cell pressure in mbar.",
    )

    panel_pressure = property_creator(
        channel=1,
        max_val=10,
        calibration=panel_pressure_calibration,
        doc="Panel pressure in mbar.",
    )

    def get_siemens_pressures(self) -> dict[str, float]:
        cell, panel = self.adapter.siemens_task.read(
            number_of_samples_per_channel=averaging)  # type: ignore
        return {
            "cellPressure": cell_pressure_calibration(np.mean(cell)),
            "panelPressure": panel_pressure_calibration(np.mean(panel)),
        }

    def _heinzinger_setter(self, high_voltage: float) -> None:
        self.set_output_voltage(heinzinger_write_calibration(high_voltage=high_voltage))

    heinzinger = property_creator(
        channel=7,
        calibration=heinzinger_read_calibration,
        doc="Heinzinger high voltage output in V.",
        fset=_heinzinger_setter,
    )

    # Output
    @property
    def output(self) -> float:
        """Voltage of the etalon driver."""
        return self._output

    @output.setter
    def output(self, value: float) -> None:
        self.set_output_voltage(value=value)

    def set_output_voltage(self, value: float) -> None:
        with nidaqmx.Task() as niTask_w:
            niTask_w.ao_channels.add_ao_voltage_chan("Dev1/ao0")
            try:
                niTask_w.write(value)
            except nidaqmx.DaqError as exc:
                log.error(f"Setting the voltage failed:  {exc}")
            else:
                self._output = value

    def set_etalon_voltage(self, value: float) -> None:
        """Set the etalon voltage in V."""
        self.output = value


def read_publish(device: NiCardInstrument, publisher: DataPublisher):
    data = device.get_siemens_pressures()
    data["highVoltage"] = device.heinzinger
    publisher.send_legacy(data)


def task(stop_event: Event) -> None:
    """The task which is run by the starter."""
    # Initialize
    with Actor("nicard", NiCardInstrument, periodic_reading=interval) as actor:
        actor.read_publish = read_publish
        actor.connect(publisher=actor.publisher)  # connect to the device

        # Continuous loop
        actor.listen(stop_event=stop_event)  # listen for commands and do the regular readouts

        # Finish
        # in listen and __exit__ included


if __name__ == "__main__":
    log.addHandler(logging.StreamHandler())
    log.setLevel(logging.INFO)

    class Signal:
        def is_set(self):
            return False
    try:
        task(Signal())  # type: ignore
    except KeyboardInterrupt:
        pass
