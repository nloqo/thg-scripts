# -*- coding: utf-8 -*-
"""
OPO oven (TC038D). "OPOoven"

Used for the self-built OPO.
"""


import logging

from pymeasure.instruments.hcp.tc038d import TC038D
from pymeasure.instruments.resources import find_serial_port

import pyvisa as pv
from pyvisa import constants

from pyleco.actors.actor import Actor
from pyleco.utils.data_publisher import DataPublisher

log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())


# Parameters
interval = 3


class TC038D_mod(TC038D):

    def ask(self, command, **kwargs):
        for _ in range(3):
            try:
                value = super().ask(command, **kwargs)
            except pv.errors.VisaIOError as exc:
                if exc.error_code == constants.StatusCode.error_serial_parity:
                    error = "parity error"
                elif exc.error_code == constants.StatusCode.error_serial_framing:
                    error = "framing error"
                elif exc.error_code == constants.StatusCode.error_serial_overrun:
                    error = "overrun error"
                else:
                    log.warning(f"Communication error {exc}")
                    self.adapter.connection.clear()
                    continue
                log.warning(f"VISA error {error}")
            except ValueError as exc:
                log.warning(f"ValueError {exc}.")
            else:
                return value
        raise ConnectionAbortedError("Reading failed after 3 tries.")


def readout(device: TC038D_mod, publisher: DataPublisher):
    publisher.send_legacy({'OPOoven': device.temperature})


def task(stop_event):
    """The task which is run by the starter."""
    # Initialize
    with Actor("OPOoven", TC038D_mod, periodic_reading=interval, log=log) as c:
        c.read_publish = readout
        c.connect(find_serial_port(4292, 60000, "1904134"))

        # Loop
        c.listen(stop_event)

        # disconnect in exit


if __name__ == "__main__":
    # Run the task if executed.
    class Signal:
        def is_set(self):
            return False
    try:
        task(Signal())
    except KeyboardInterrupt:
        pass
