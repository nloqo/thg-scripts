"""
Read the CO2 sensor.
"""

import logging
from time import sleep

from pyleco.utils.data_publisher import DataPublisher
from pyleco.utils.events import Event, SimpleEvent
try:
    from pymeasure.instruments.resources import find_serial_port
except ImportError:
    from devices.findDevices import find_serial_port

from devices.sensirion import SCD30

# Parameters
hw_info = (0x0403, 0x6001, "A50285BIA")  # ftdi
readout_interval = 0.3  # s


log = logging.getLogger(__name__)


class ExtendedSimpleEvent(SimpleEvent):
    def wait(self, timeout: float) -> bool:
        sleep(timeout)
        return self.is_set()


def task(stop_event: Event):
    # init
    port = find_serial_port(*hw_info)
    device = SCD30(port)
    publisher = DataPublisher(full_name="MYRES.co2sensor")
    try:
        device.start_measurement()
    except Exception as exc:
        log.exception("Starting measurement failed.", exc_info=exc)
        raise

    log.info("CO2 reader initialized")
    # loop
    while not stop_event.wait(readout_interval):
        if device.data_ready:
            co2, temp, hum = device.get_values()
            publisher.send_legacy({"co2": co2, "BoxTemp": temp, "humidity2": hum})
            print(co2, temp, hum)
        else:
            print("no data")

    # finish
    device.stop_measurement()
    device.shutdown()


if __name__ == "__main__":
    try:
        task(stop_event=ExtendedSimpleEvent())
    except KeyboardInterrupt:
        pass
