# -*- coding: utf-8 -*-
"""
Read the high_finesse Wavemeters.
"""


import logging
import time

from devices import high_finesse
from pyleco.utils.data_publisher import DataPublisher

log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())


# Parameters
devices = {3945: "signal", 598: "pump"}  # dictionary of the serial numbers and publishing names
default_name = "wavemeter"  # default name if serial number is unknown.
interval = 1  # s, interval between check for over/under-exposure or time in normal mode
auto_exposure = False  # adjust automatically the exposure time
switching_enabled: bool = False  # Switch between both wavelength regions.
alternative_name: str = "pumpSHG"  # for values below 1000 nm
alternative_interval: int = 30


def callback(version, mode, intVal, dblVal, Res1=None):
    """Callback method."""
    if mode in (high_finesse.wlmConst.cmiWavelength1,
                high_finesse.wlmConst.cmiWavelength2,
                high_finesse.wlmConst.cmiWavelength3,
                high_finesse.wlmConst.cmiWavelength4,
                high_finesse.wlmConst.cmiWavelength5,
                high_finesse.wlmConst.cmiWavelength6,
                ):
        show_wavelength(version, intVal, dblVal)
        # log.debug(("Wavelength received", version, mode, intVal, dblVal))
    elif mode in (high_finesse.wlmConst.cmiFrequency1,
                  high_finesse.wlmConst.cmiFrequency2,
                  ):
        show_frequency(version, intVal, dblVal)
        # log.debug(("Frequency received", version, mode, intVal, dblVal))
    elif mode == high_finesse.wlmConst.cmiPower:
        show_power(version, intVal, dblVal)
        # log.debug(("Power received", version, mode, intVal, dblVal))
    else:
        log.debug(("Data received", version, mode, intVal, dblVal))


def show_wavelength(version, timestamp, value):
    """Show the received wavelength."""
    name = devices.get(version, default_name)
    value = value if value > 0 else float("nan")
    publisher.send_legacy({f"{name}_wl": value if value > 0 else float("nan")})


def show_frequency(version, timestamp, value):
    """Show the received frequency."""
    name = devices.get(version, default_name)
    publisher.send_legacy({f"{name}_f": value})


def show_power(version, timestamp, value):
    """Show the received power."""
    name = devices.get(version, default_name)
    publisher.send_legacy({f"{name}_P": value})


def task(stop_event):
    """The task which is run by the starter."""
    # Initialize
    global publisher
    publisher = DataPublisher(full_name="wavemeterReader", log=log)
    try:
        wlm = high_finesse.Wavemeter()
    except Exception as exc:
        log.exception("Wavemeter connection error.", exc_info=exc)
        return
    log.info(f"Wavemeter reader started. {wlm.wlm_count} wavemeters found.")
    wlm.setup_callback(callback)

    next_loop = interval
    original_name = devices[3945]

    # Continuous loop
    while not stop_event.wait(next_loop):  # as an alternative to sleep(1)
        if auto_exposure:
            try:
                power = wlm.power
            except Exception as exc:
                log.exception("Reading power failed.", exc_info=exc)
            else:
                if power == float("inf") or power == float("nan"):
                    wlm.exposureMode = True
                else:
                    wlm.exposureMode = False
        if switching_enabled:
            if next_loop == interval:
                # switch to lower range
                wlm.range = 6
                devices[3945] = alternative_name
                next_loop = alternative_interval
            else:
                # switch to upper range:
                wlm.range = 7
                devices[3945] = original_name
                next_loop = interval

    # Finish
    wlm.setup_callback()


if __name__ == "__main__":
    """Run the task if the module is executed."""
    if len(log.handlers) < 2:
        log.addHandler(logging.StreamHandler())
    log.setLevel(logging.INFO)

    class Signal:
        def is_set(self):
            return False

        def wait(self, interval):
            time.sleep(interval)
            return False

    try:
        task(Signal())
    except KeyboardInterrupt:
        pass
