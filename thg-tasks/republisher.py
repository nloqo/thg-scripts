# -*- coding: utf-8 -*-
"""
Task starting a republisher.
"""


import logging
import time
from typing import Any, Callable

from pyleco_extras.utils.republisher import Republisher


log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())


# Parameters
handlings: dict[str, tuple[Callable[[Any], Any], str]] = {
    # "old key": (conversion function, "new key"),  # how to transform some values to other ones.
    # "OPA1pump": (
    #     lambda x: 2011 / (2 * 512) - ((2011 / (2 * 512)) ** 2 - x / 50e-9 / 512) ** 0.5,
    #     "OPA1pump_E",
    # ),  # adjustment for Wb instead of Pixelsumme. Wb -> mJ
    "OPA1pump": (
        lambda x: 9.0814707e4 * x**2 + 9.76784132 * x + 1.03903444e-5,
        "OPA1pump_E",
    ),  # 2024-02-29 Kalibrierung der OPA1 Pump-Photodiode
    # "OPA2pump": (
    #     lambda x: 4.42375386e06 * x**2 + 2.23635103e02 * x + 1.53973807e-03,
    #     "OPA2pump_E",
    # ),  # source 10.5.2023 "OPA2 Pumpkalibrierung"
    "OPA2pump": (
        lambda x: 109371.342 * x + 0.00747267,
        "OPA2pump_E",
    ),  # source 21.02.2025 "Kalibrierung OPA2 Photodiode Energieüberwachung" #21955
    # "idler2": (
    #     lambda x: 2.608e08 * x**2 + 97.2422 * x,
    #     "idler2_E",
    # ),  # 22.03.2024 Kalibrierung OPA2 Photodiode (Wb-> J)
    # "idler2": (
    #     lambda x: 7.19299e13 * (x - 4.87024e-08) ** 3
    #     + 3.83689e07 * (x - 4.87024e-08) ** 2
    #     + 170.001 * (x - 4.87024e-08),
    #     "idler2_E",
    # ),  # 25.03.2024
    "idler2": (
        lambda x: 4.44329e+13 * (x - 2.27582e-08) ** 3
        + 8.71189e+07 * (x - 2.27582e-08) ** 2
        + 163.135 * (x - 2.27582e-08),
        "idler2_E",
    ),  # 11.04.2024
    "OPO_pump": (
        # lambda x: 3.93521552e03 * x - 1.75509653e-04,  # 2024-08-06 "Aufbau ns-OPO Strahlen": Weiterführung 2024-08-12, Messung 2024_08_13T14_37_44
        lambda x: 2.64613041e+04 * x - 1.33999633e-03,  # #20474 ns-OPO in OPA2 Erster Test Messung 2025_01_24T10_28_17
        "OPO_pump_E",
    ),
    # Wb --> J
    "OPO_pump_532": (
        # lambda x: 1.26474237e03 * x - 3.48120016e-04,  # 2024-08-06 "Aufbau ns-OPO Strahlen": Weiterführung 2024-08-12, Messung 2024_08_30T16_40_28
        lambda x: 2.19285508e03 * x - 1.18627500e-04,  # #19432 Messung 2024_12_17T10_54_27
        "OPO_pump_532_E",
    ),
    # Wb --> J
}


def task(stop_event):
    """The task which is run by the starter."""
    # Initialize
    republisher = Republisher(name="Republisher", handlings=handlings, log=log)
    republisher.start_listen(stop_event=stop_event)


if __name__ == "__main__":
    # Run the task if executed.
    class Signal:
        def is_set(self):
            return False

        def wait(self, timeout):
            time.sleep(timeout)

    try:
        task(Signal())
    except KeyboardInterrupt:
        pass
