from pyleco.directors.data_logger_director import DataLoggerDirector


name = 'DataLoggerEnergies'


configuration = {
    'trigger_type': "timer",
    'trigger_timeout': 0.05,
    'trigger_variable': "Picoscope",
    'variables': ['time', 'signal_wl', 'signal_P', 'pump_wl', 'seedWavelengthCalculated', 'precisionPressure', 'Nova2', 'Picoscope', 'OPA1pump', 'OPA2pump', 'idler', 'idler-cw', 'idler2', 'idler2-cw', 'Quanta-Ray_E', 'OPA2PumpDepleted_E', 'OPA1pump_E', 'OPA2pump_E', 'idler2_E'],
    'units': {'signal_wl': 'nm', 'signal_P': 'uW', 'pump_wl': 'nm', 'seedWavelengthCalculated': 'nm', 'precisionPressure': 'mbar', 'Nova2': 'J', 'idler': 'Wb', 'idler2': 'Wb', 'OPA1pump_E': 'mJ', 'OPA2pump_E': 'J', 'idler2_E': 'J'},
    'valuing_mode': "last",
    'value_repeating': False,
}


gui_configuration = {'header': '', 'unitsText': '', 'autoSave': False, 'pause': False}


plot_configuration = [
    {'name': 'Plot 0', 'type': 'MultiPlotWidget', 'x_key': 'index', 'autoCut': 200, 'ly': False, 'lg': False, 'vls': False, 'evaluation': True, 'lines': {'time': '', 'signal_wl': '', 'signal_P': '', 'pump_wl': '', 'seedWavelengthCalculated': '', 'precisionPressure': '', 'Nova2': '', 'Picoscope': '', 'OPA1pump': '1,r', 'OPA2pump': 'g', 'idler': '', 'idler-cw': '', 'idler2': '', 'idler2-cw': '', 'Quanta-Ray_E': '2,d', 'OPA2PumpDepleted_E': '', 'OPA1pump_E': '', 'OPA2pump_E': '', 'idler2_E': ''}, 'y_key': 'OPA1pump_E'},
    {'name': 'Plot 1', 'type': 'MultiPlotWidget', 'x_key': 'index', 'autoCut': 200, 'ly': False, 'lg': False, 'vls': False, 'evaluation': False, 'lines': {'time': '', 'signal_wl': '', 'signal_P': '', 'pump_wl': '', 'seedWavelengthCalculated': '', 'precisionPressure': '', 'Nova2': '', 'Picoscope': '', 'OPA1pump': '1,d', 'OPA2pump': '', 'idler': '', 'idler-cw': '', 'idler2': 'g', 'idler2-cw': '', 'Quanta-Ray_E': '2,b', 'OPA2PumpDepleted_E': '', 'OPA1pump_E': '', 'OPA2pump_E': '', 'idler2_E': ''}, 'y_key': 'OPA2pump_E'},
    {'name': 'Plot 2', 'type': 'SinglePlotWidget', 'x_key': 'index', 'autoCut': 200, 'ly': 2.4000604147480382e-05, 'lg': False, 'vls': False, 'evaluation': True, 'y_key': 'idler2_E', 'dots': False, 'lmm': True, 'mm': False},
]


if __name__ == '__main__':
    with DataLoggerDirector(name='call' + name, actor=name) as d:
        # print(d.save_data())
        print(d.ask_rpc(method='set_configuration', configuration=gui_configuration))
        print(d.start_collecting(**configuration))
        print(d.ask_rpc(method='set_plot_configuration', plot_configuration=plot_configuration))
