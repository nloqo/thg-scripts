from pyleco.directors.data_logger_director import DataLoggerDirector


name = 'DataLoggerOPO'


configuration = {
    'trigger_type': "timer",
    'trigger_timeout': 1.0,
    'trigger_variable': "",
    'variables': ['time', 'OPOBlockStab', 'OPOBlock', 'OPOBoxOutside', 'OPOoven', 'humidityTemp', 'humidity', 'signal_wl', 'signal_P', 'OPA1oven', 'OPOBlockIntegral', 'seedWavelengthCalculated', 'co2', 'humidity2', 'BoxTemp'],
    'valuing_mode': "last",
    'value_repeating': False,
}


gui_configuration = {'header': '', 'unitsText': 'OPOBlockStab: degreeC, OPOBlock: degreeC, OPOBoxOutside: degreeC, humidityTemp: degreeC, signal_wl: nm, signal_P: µW, OPOBlockIntegral: ms, BoxTemp: degreeC', 'autoSave': False, 'pause': False}


plot_configuration = [
    {'name': 'Plot 0', 'type': 'MultiPlotWidget', 'x_key': 'index', 'autoCut': 1000, 'lines': {'time': '', 'OPOBlockStab': 'r', 'OPOBlock': 'darkred', 'OPOBoxOutside': '1,lightgreen', 'OPOoven': '', 'humidityTemp': '1,darkgreen', 'humidity': '2,cyan', 'signal_wl': '', 'signal_P': '', 'OPA1oven': '', 'OPOBlockIntegral': '', 'seedWavelengthCalculated': '', 'co2': '', 'humidity2': '2,blue', 'BoxTemp': '1,g'}, 'y_key': 'OPOBoxOutside'},
    {'name': 'Plot 1', 'type': 'MultiPlotWidget', 'x_key': 'index', 'autoCut': 1000, 'lines': {'time': '', 'OPOBlockStab': '', 'OPOBlock': '', 'OPOBoxOutside': '', 'OPOoven': '', 'humidityTemp': '', 'humidity': '', 'signal_wl': 'g', 'signal_P': '1,r', 'OPA1oven': '', 'OPOBlockIntegral': '', 'seedWavelengthCalculated': '', 'co2': '', 'humidity2': '', 'BoxTemp': ''}, 'y_key': 'signal_wl'},
    {'name': 'Plot 2', 'type': 'SinglePlotWidget', 'x_key': 'index', 'autoCut': 1000, 'y_key': 'co2', 'dots': False, 'lmm': False, 'mm': False, 'ly': False, 'lg': False},
]


if __name__ == '__main__':
    with DataLoggerDirector(name='call' + name, actor=name) as d:
        # print(d.save_data())
        print(d.ask_rpc(method='set_configuration', configuration=gui_configuration))
        print(d.start_collecting(**configuration))
        print(d.ask_rpc(method='set_plot_configuration', plot_configuration=plot_configuration))
